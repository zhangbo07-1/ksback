<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\EmailLog;
use App\Models\User;
use Mail;
class SendEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $username,$useremail, $emailTitle,$emailBody,$emailLog,$hostname;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()//EmailTemplet $email, User $user)
    {
/*
        $this->username = $user->name;
        $this->useremail= $user->email;
        $this->emailTitle = $email->title;
        $this->emailBody = $email->body;
*/
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->attempts() > 2)
        {
              $this->emailLog->status = 1;
          $this->emailLog->save();
        }


        $username = $this->username;
        $useremail = $this->useremail;
        $emailTitle = $this->emailTitle;
        $emailBody = $this->emailBody;
if($this->attempts() == 1)
{
        $this->emailLog = new EmailLog;
        $this->emailLog->username = $username;
$this->emailLog->useremail = $useremail;
        $this->emailLog->emailTitle = $emailTitle;
        $this->emailLog->emailBody = $emailBody;
$this->emailLog->hostname = $this->hostname;
        $this->emailLog->save();

}

        Mail::send('emails.temple', [ 'user' => $username, 'emailTitle' => $emailTitle,'emailBody'=>$emailBody ], function ( $m ) use ( $username,$useremail,$emailTitle )
            {
                $m->from('postmaster@knowsurface.com','知识平面Knowsurface')->to($useremail,$username)->subject($emailTitle);
            });
     $this->emailLog->status = 2;
  $this->emailLog->save();

    }
}
